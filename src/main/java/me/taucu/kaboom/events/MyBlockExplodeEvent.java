package me.taucu.kaboom.events;

import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockExplodeEvent;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Used internally to ensure everything handles the explosion before KaBoom
 */
public class MyBlockExplodeEvent extends BlockExplodeEvent {

    public MyBlockExplodeEvent(@NotNull Block what, @NotNull List<Block> blocks, float yield) {
        super(what, blocks, yield);
    }

    public @NotNull HandlerList getHandlers() {
        return super.getHandlers();
    }

    public static @NotNull HandlerList getHandlerList() {
        return BlockExplodeEvent.getHandlerList();
    }

}
