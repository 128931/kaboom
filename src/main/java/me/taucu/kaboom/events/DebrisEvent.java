package me.taucu.kaboom.events;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.physics.ExplosionHandler;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.entity.EntityEvent;
import org.bukkit.persistence.PersistentDataType;

import java.util.Objects;

public abstract class DebrisEvent extends EntityEvent {

    public DebrisEvent(FallingBlock debris) {
        super(debris);
    }

    /**
     * Gets the debris causing this event
     * @return the FallingBlock debris
     * @apiNote same as {@link EntityEvent#getEntity()} but casts to the correct type
     */
    public FallingBlock getDebris() {
        return (FallingBlock) entity;
    }

    /**
     * Tests if the Debris was in visual mode when it was spawned<br>
     * @return true if it was in visual mode, false otherwise
     */
    public boolean isVisualMode() {
        return ExplosionHandler.isVisualMode(
                Objects.requireNonNull(getDebris().getPersistentDataContainer().get(
                        KaBoom.get().getHandler().getFallingKey(), PersistentDataType.BYTE_ARRAY
                ), "get must not be null")
        );
    }

}
