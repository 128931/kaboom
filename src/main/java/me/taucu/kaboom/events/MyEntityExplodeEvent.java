package me.taucu.kaboom.events;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Used internally to ensure everything handles the explosion before KaBoom
 */
public class MyEntityExplodeEvent extends EntityExplodeEvent {

    public MyEntityExplodeEvent(Entity what, Location location, List<Block> blocks, float yield) {
        super(what, location, blocks, yield);
    }

    public @NotNull HandlerList getHandlers() {
        return super.getHandlers();
    }

    public static @NotNull HandlerList getHandlerList() {
        return EntityExplodeEvent.getHandlerList();
    }

}
