package me.taucu.kaboom.util;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class RayBouncer {

    private RayBouncer() {}
    
    /**
     * Casts a Block-intersecting ray that ignores {@link Block#isPassable}
     * blocks, bouncing one it hits an impassible block.
     * 
     * @param toIgnore    {@link HashSet} of blocks for the ray to ignore
     * @param loc         {@link Location} start of the ray
     * @param vel         {@link Vector} direction of the ray (retains magnitude)
     * @param maxBounces  maximum bounces of this operation
     * @param bounceRange the maximum range of each bounce
     * @return {@link RayBounceResult}
     */
    public static RayBounceResult rayBounce(Set<Block> toIgnore, Location loc, Vector vel, int maxBounces, int bounceRange) {
        BlockIterator it = new BlockIterator(loc.getWorld(), loc.toVector(), vel, 0, bounceRange);
        Block prevBlock = loc.getBlock();
        Block lastFree = prevBlock;
        int bounces = 0;
        boolean foundFree = true;
        
        while (it.hasNext()) {
            final Block b = it.next();
            // loc.getWorld().spawnParticle(Particle.REDSTONE, b.getLocation(), 1, new Particle.DustOptions(Color.RED, 1));
            
            if (!b.isPassable() && !toIgnore.contains(b)) {
                if (bounces < maxBounces) {
                    bounce(Objects.requireNonNull(b.getFace(prevBlock), "getFace must not be null").getDirection(), vel);
                    bounces++;
                    it = new BlockIterator(loc.getWorld(), prevBlock.getLocation().toCenterLocation().toVector(), vel, 0, bounceRange);
                } else {
                    foundFree = false;
                    break;
                }
            } else if (!prevBlock.isPassable() && !toIgnore.contains(prevBlock)) {
                lastFree = b;
            }
            prevBlock = b;
        }
        
        return new RayBounceResult(lastFree.getLocation(), vel, foundFree);
    }
    
    /**
     * Bounces a {@link Vector} off of a surface
     * 
     * @param planeNormal Normalized {@link Vector} representing the surface to bounce off of
     * @param vel         The {@link Vector} to bounce
     */
    public static void bounce(Vector planeNormal, Vector vel) {
        vel.subtract(planeNormal.multiply(vel.dot(planeNormal) * 2.0D));
    }

    public record RayBounceResult(Location loc, Vector vel, boolean foundFree) {

        public Location getLocation() {
            return loc;
        }

        public Vector getVelocity() {
            return vel;
        }

        public boolean foundFreeVector() {
            return foundFree;
        }
    }
    
}
