package me.taucu.kaboom.util;

import org.bukkit.inventory.ItemStack;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class ItemSerial {
    
    private ItemSerial() {
    }
    
    /**
     * Serializes ItemStack(s) to a {@link #wrap(List) wrapped} byte array
     * 
     * @param it ItemStack(s) to iterate over
     * @return A {@link #wrap(List) wrapped} byte array of serialized ItemStack(s)
     */
    public static byte[] serialize(Iterable<ItemStack> it) {
        final ArrayList<byte[]> stacks = new ArrayList<>();
        for (final ItemStack i : it) {
            if (i != null && !i.getType().isAir()) {
                stacks.add(i.serializeAsBytes());
            }
        }
        return wrap(stacks);
    }
    
    /**
     * Deserializes a {@link #wrap(List) wrapped} byte array of item(s) to
     * ItemStack(s)
     * 
     * @param data the {@link #wrap(List) wrapped} byte array of item(s)
     * @return the deserialized item(s)
     */
    public static ArrayList<ItemStack> deserialize(byte[] data) {
        final ArrayList<ItemStack> stacks = new ArrayList<>();
        for (final byte[] b : unwrap(data)) {
            stacks.add(ItemStack.deserializeBytes(b));
        }
        return stacks;
    }
    
    /**
     * Wraps multiple byte arrays into 1 array.
     * <p>
     * The data is wrapped by the following means:<br>
     * [32bit int for array1 length][array1...][32bit int for array2
     * length][array2...]... and so on
     *
     * @param bytes A list of arrays to wrap
     * @return the wrapped arrays
     */
    static byte[] wrap(List<byte[]> bytes) {
        int size = 0;
        for (final byte[] ba : bytes) {
            size += ba.length + 4;
        }
        final ByteBuffer buf = ByteBuffer.allocate(size);
        for (final byte[] ba : bytes) {
            buf.putInt(ba.length);
            buf.put(ba);
        }
        return buf.array();
    }
    
    /**
     * Unwraps a byte array to one or more byte arrays
     * <p>
     * The data is unwrapped by the following means:<br>
     * [32bit int for array1 length][array1...][32bit int for array2
     * length][array2...]... and so on
     *
     * @param bytes Array of bytes to unwrap
     * @return the unwrapped bytes
     */
    static List<byte[]> unwrap(byte[] bytes) {
        final ByteBuffer buf = ByteBuffer.wrap(bytes);
        final ArrayList<byte[]> lst = new ArrayList<>();
        while (buf.remaining() > 0) {
            final int dataLength = buf.getInt();
            final byte[] data = new byte[dataLength];
            buf.get(data, 0, dataLength);
            lst.add(data);
        }
        return lst;
    }
    
}
