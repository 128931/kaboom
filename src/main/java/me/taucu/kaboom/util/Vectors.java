package me.taucu.kaboom.util;

import java.util.Random;

import org.bukkit.util.Vector;

public class Vectors {
    
    public static final Random rand = new Random();
    
    /**
     * Creates a random double within two bounds
     * 
     * @param min The minimum bound
     * @param max The maximum bound
     * @return the result
     */
    public static double randBoundDouble(double min, double max) {
        return (rand.nextDouble() * (max - min)) + min;
    }
    
    /**
     * Create a random {@link Vector} within defined bounds
     * 
     * @param min The maximum negative bound
     * @param max The maximum positive bound
     * @return the created {@link Vector}
     */
    public static Vector randomVector(double min, double max) {
        return new Vector(randBoundDouble(-min, max), randBoundDouble(-min, max), Vectors.randBoundDouble(-min, max));
    }
    
}
