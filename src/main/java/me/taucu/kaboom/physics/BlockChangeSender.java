package me.taucu.kaboom.physics;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.Closeable;
import java.util.ArrayList;

public class BlockChangeSender implements Closeable {

    private final ArrayList<Block>[] ring;
    private final int delay;

    private int pos = 0;

    private int taskId;

    @SuppressWarnings("unchecked")
    public BlockChangeSender(JavaPlugin pl, int delay) {
        this.delay = delay;
        this.ring = (ArrayList<Block>[]) new ArrayList[delay];
        for (int i = 0; i < ring.length; i++) {
            ring[i] = new ArrayList<>();
        }
        this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, this::tick, 0, 1);
    }

    public void queueBlock(Block block) {
        ring[pos].add(block);
    }

    private void tick() {
        pos++;
        pos = pos % ring.length;
        final ArrayList<Block> current = ring[pos];
        if (!current.isEmpty()) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                Location playerLoc = player.getLocation();
                for (Block block : current) {
                    Location loc = block.getLocation();
                    int distanceSquared = square(16 + (loc.getWorld().getViewDistance() << 4));
                    if (playerLoc.getWorld() == block.getWorld() && square(block.getX() - playerLoc.getX()) < distanceSquared && square(block.getZ() - playerLoc.getZ()) < distanceSquared) {
                        // System.out.println("sent to " + player.getName() + " distance=" + playerLoc.distance(block.getLocation()));
                        player.sendBlockChange(loc, block.getBlockData());
                    }

                }
            }
            ring[pos] = new ArrayList<>();
        }
    }

    @Override
    public void close() {
        if (taskId != -1) {
            Bukkit.getScheduler().cancelTask(taskId);
            taskId = -1;
        }
    }

    public int getDelay() {
        return delay;
    }

    public static int square(int a) {
        return a * a;
    }

    public static double square(double a) {
        return a * a;
    }

}
