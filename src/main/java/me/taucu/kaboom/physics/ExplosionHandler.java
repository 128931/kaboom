package me.taucu.kaboom.physics;

import me.taucu.kaboom.KaBoom;
import me.taucu.kaboom.events.DebrisBreakEvent;
import me.taucu.kaboom.events.DebrisLandEvent;
import me.taucu.kaboom.events.ExplosionHandledEvent;
import me.taucu.kaboom.util.ItemSerial;
import me.taucu.kaboom.util.RayBouncer;
import me.taucu.kaboom.util.RayBouncer.RayBounceResult;
import me.taucu.kaboom.util.Vectors;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.TileState;
import org.bukkit.block.data.BlockData;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Vector;

import java.io.Closeable;
import java.util.*;
import java.util.logging.Level;

public class ExplosionHandler implements Listener, Closeable {
    
    private static final BlockData AIR_DATA = Material.AIR.createBlockData();
    private static final byte[] VISUAL_ONLY_DATA = {-1, 0}; // [0]=-1 alternate mode v1; [1]=0 visual only;
    private static final Random rand = new Random();

    private final Vector toCopyVector = new Vector();
    private final BoundingBox toCopyBox = new BoundingBox();

    private final HashSet<Material> disabledFallingBlocks = new HashSet<>();
    private final HashSet<Material> disabledFallingBlocksPlacement = new HashSet<>();
    private final HashSet<Material> disabledFallingBlockDrops = new HashSet<>();

    private final ItemStack tool = new ItemStack(Material.DIAMOND_PICKAXE);

    private final KaBoom kaboom;
    private final NamespacedKey fallingKey;

    private BlockChangeSender blockChangeSender;

    private boolean silkTouch = false;

    private int maxRayAttempts = 5;
    private int maxRayDistance = 10;
    private double breakChanceNoFree = 80;

    private double velocityMultiplier = 2;
    private double velocityRandom = 0.2;
    
    private boolean fallingBlockBreakAnimation = true;
    private boolean fallingBlocksHurtEntities = true;
    private boolean fallingBlockVisualOnlyMode = false;

    private long explodedBlocks = 0;

    public ExplosionHandler(KaBoom pl) {
        this.kaboom = pl;
        fallingKey = new NamespacedKey(pl, "ItemDrop");
        this.blockChangeSender = new BlockChangeSender(pl, 5);
    }
    
    /**
     * Called to modify entity/block explosion events and will not work properly by itself
     * 
     * @param center The center {@link Location} of the blast
     * @param toBlow {@link List} of blocks that are to be blown up
     */
    public void handleExplosion(Location center, List<Block> toBlow) {
        final Vector centerVec = center.toVector();
        final HashSet<Block> allBlocks = new HashSet<>(toBlow);
        toBlow.clear();

        final ArrayList<Block> toBreak = new ArrayList<>(allBlocks.size());
        final ArrayList<FallingBlock> allDebris = new ArrayList<>(allBlocks.size());

        for (Block block : allBlocks) {
            Material type = block.getType();
            if (type == Material.TNT || type.isAir()) {
                toBlow.add(block);
            } else if (block.getState() instanceof TileState || disabledFallingBlocks.contains(type)) {
                toBreak.add(block);
            } else {
                FallingBlock debris = createDebris(centerVec, block, type, allBlocks, breakChanceNoFree);
                if (debris == null) {
                    toBreak.add(block);
                } else {
                    allDebris.add(debris);
                }
            }
        }

        new ExplosionHandledEvent(center.getWorld(), this, center, allBlocks, toBreak, toBlow, allDebris).callEvent();

        this.explodedBlocks += (toBreak.size() + toBlow.size() + allDebris.size());

        for (Block block : toBreak) {
            if (silkTouch) {
                block.breakNaturally(tool, false);
            } else {
                toBlow.add(block);
            }
        }
    }

    /**
     * Used internally to create debris also sets the target block to air if successful
     * @param centerVec the center of the explosion (midpoint of block)
     * @param block the {@link Block} to create debris for
     * @param type the {@link Material} of the block (optimization to reduce getType calls)
     * @param ignoreInTrace the {@link HashSet<Block>} of blocks to ignore in raytrace operations
     * @return the {@link FallingBlock} debris or null
     */
    public FallingBlock createDebris(Vector centerVec, Block block, Material type, HashSet<Block> ignoreInTrace, double breakChanceNoFree) {
        final Collection<ItemStack> drops = block.getDrops(tool);
        BlockData fallingBlockData;
        // if it only has one drop and that drop is not itself and is also a block, set the type of the falling block to that type
        if (drops.size() == 1) {
            final Material dropType = drops.stream().findFirst().get().getType();
            if (dropType != type && dropType.isBlock() && !dropType.isAir()) {
                fallingBlockData = Bukkit.createBlockData(dropType);
            } else {
                fallingBlockData = block.getBlockData();
            }
        } else {
            fallingBlockData = block.getBlockData();
        }

        final Location centerLoc = block.getLocation().toCenterLocation();
        final Vector velocity = calculateVelocity(centerVec, centerLoc);

        final RayBounceResult bounceResult = RayBouncer.rayBounce(ignoreInTrace, centerLoc, velocity, maxRayAttempts, maxRayDistance);

        if (breakChanceNoFree > 0 && !bounceResult.foundFreeVector() && rand.nextDouble() * 100 < breakChanceNoFree) {
            return null;
        } else {
            block.setBlockData(AIR_DATA, true);
            FallingBlock debris = spawnFallingBlock(
                    centerFallingBlock(bounceResult.getLocation()),
                    fallingBlockData,
                    Vectors.randomVector(velocityRandom, velocityRandom)
                            .add(bounceResult.getVelocity()));
            if (fallingBlockVisualOnlyMode) {
                debris.getPersistentDataContainer().set(fallingKey, PersistentDataType.BYTE_ARRAY, VISUAL_ONLY_DATA);
                dropItems(debris, drops);
            } else {
                debris.getPersistentDataContainer().set(fallingKey, PersistentDataType.BYTE_ARRAY, ItemSerial.serialize(drops));
            }
            return debris;
        }
    }
    
    /**
     * Used internally to calculate the correct velocity away from the explosion source
     * @param centeredSourceLoc the centered source location
     * @param centeredBlockLoc the centered target block location
     * @return the calculated vector
     */
    public Vector calculateVelocity(Vector centeredSourceLoc, Location centeredBlockLoc) {
        return toCopyVector.copy(centeredSourceLoc)
                .subtract(centeredBlockLoc.toVector())
                .normalize()
                .multiply(-velocityMultiplier);
    }
    
    /**
     * Spawns a {@link FallingBlock}
     * 
     * @param loc      {@link Location} to spawn the {@link FallingBlock}
     * @param data     {@link BlockData} of the {@link FallingBlock}
     * @param velocity A {@link Vector} representing the velocity of the
     *                 {@link FallingBlock}
     * @return The {@link FallingBlock}
     */
    public FallingBlock spawnFallingBlock(Location loc, BlockData data, Vector velocity) {
        FallingBlock falling = loc.getWorld().spawnFallingBlock(loc, data);
        falling.setPersistent(true);
        falling.setVelocity(velocity);
        return falling;
    }
    
    /**
     * Centers a {@link Location} for {@link FallingBlock}s relative to the world
     * grid.
     * 
     * @param loc The location to center
     * @return The {@link FallingBlock} specific centered {@link Location}
     */
    public Location centerFallingBlock(Location loc) {
        loc.setX(loc.getBlockX() + 0.5);
        loc.setY(loc.getY() + 0.0014); // special offset for falling sand 0.0014
        loc.setZ(loc.getBlockZ() + 0.5);
        return loc;
    }

    /**
     * Drops all items naturally in the world if each item isItem() and is not air
     * @param src the debris to drop the items from, inheriting velocity
     * @param itemStacks the items to drop
     */
    public void dropItems(FallingBlock src, Collection<ItemStack> itemStacks) {
        Location loc = src.getBoundingBox().getCenter().toLocation(src.getWorld());
        for (ItemStack is : itemStacks) {
            Material type = is.getType();
            if (type.isItem() && !type.isAir()) {

                loc.getWorld().dropItem(loc, is).setVelocity(src.getVelocity());
            }
        }
    }

    /**
     * Safely deserializes the item drops of non-visual data
     *
     * @param serialized the serialised items
     * @return the deserialized items or an empty list of an error occurred
     */
    public ArrayList<ItemStack> deserializeDebrisDrops(byte[] serialized) {
        try {
            return ItemSerial.deserialize(serialized);
        } catch (Exception ex) {
            kaboom.getLogger().log(Level.SEVERE, "error while deserializing items. data=\"" + Arrays.toString(serialized), ex);
        }
        return new ArrayList<>();
    }
    
    /**
     * checks if this debris was created in visual mode
     * @param serialized the non-null data to check
     * @return true if visual, false otherwise
     */
    public static boolean isVisualMode(byte[] serialized) {
        //                                        [0]=-1 alternate mode  [1]=0 visual mode
        return serialized.length > 1 && serialized[0] == -1 && serialized[1] == 0;
    }
    
    /**
     * Called after derbies land to apply animations etc
     * @param fb the falling block
     * @param didBreak If the falling block broke when it landed
     */
    public void postFall(FallingBlock fb, boolean didBreak) {
        final Location loc = fb.getLocation();
        final BlockData data = fb.getBlockData();
        final SoundGroup group = data.getSoundGroup();
        loc.getWorld().playSound(loc, group.getBreakSound(), SoundCategory.BLOCKS, group.getVolume() * 1.5F, group.getPitch());
        loc.getWorld().playSound(loc, Sound.ENTITY_PLAYER_ATTACK_CRIT, SoundCategory.BLOCKS, 0.25F, 0.8F);
        if (didBreak && fallingBlockBreakAnimation) {
            loc.getWorld().spawnParticle(Particle.BLOCK_DUST, loc.add(0, 0.5, 0), 50, 0.25, 0.25, 0.25, data);
        }
        if (fallingBlocksHurtEntities) {
            BoundingBox box = fb.getBoundingBox();
            // World#getNearbyEntities doesn't calculate intersections properly, so we need to make a fuzzy bounding box
            // ... because of course we do
            BoundingBox fuzzyBox = toCopyBox.copy(box).expand(3, 3, 3);
            for (Entity ent : loc.getWorld().getNearbyEntities(fuzzyBox)) {
                // we don't want to squash items as it may squash its own item.
                if (ent instanceof LivingEntity lent && box.overlaps(ent.getBoundingBox())) {
                    Material type = fb.getBlockData().getMaterial();
                    
                    double base = type.isOccluding() ? 1.5 : 0.5;
                    
                    double damage = ((type.getBlastResistance() * (base * 1.7)) + (type.getHardness() * (base * 3.8)));
                    
                    lent.damage(damage, fb);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFallingBlockLand(EntityChangeBlockEvent e) {
        if (e.getEntity() instanceof FallingBlock fb && e.getTo() != Material.AIR) {
            final PersistentDataContainer cont = fb.getPersistentDataContainer();
            final byte[] serialized = cont.get(fallingKey, PersistentDataType.BYTE_ARRAY);

            blockChangeSender.queueBlock(e.getBlock());
            
            if (serialized != null) {
                if (isVisualMode(serialized)) {
                    e.setCancelled(true);
                    DebrisBreakEvent dbe = new DebrisBreakEvent(fb);
                    if (dbe.callEvent()) {
                        if (dbe.hasDrops()) {
                            dropItems(fb, dbe.getDrops());
                        }
                        postFall(fb, true);
                    }
                } else if (disabledFallingBlocksPlacement.contains(e.getTo())) {
                    e.setCancelled(true);
                    if (disabledFallingBlockDrops.contains(fb.getBlockData().getMaterial())) {
                        DebrisBreakEvent dbe = new DebrisBreakEvent(fb);
                        if (dbe.callEvent()) {
                            if (dbe.hasDrops()) {
                                dropItems(fb, dbe.getDrops());
                            }
                            postFall(fb, true);
                        }
                    } else {
                        DebrisBreakEvent dbe = new DebrisBreakEvent(fb, deserializeDebrisDrops(serialized));
                        if (dbe.callEvent()) {
                            dropItems(fb, dbe.getDrops());
                            postFall(fb, true);
                        }
                    }
                } else if (new DebrisLandEvent(fb, e).callEvent()) {
                    postFall(fb, false);
                } else {
                    e.setCancelled(true);
                }
            }
            
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFallingBlockDropItem(EntityDropItemEvent e) {
        if (e.getEntity() instanceof FallingBlock fb) {
            final byte[] serialized = fb.getPersistentDataContainer().get(fallingKey, PersistentDataType.BYTE_ARRAY);
            if (serialized != null) {
                e.setCancelled(true);
                if (!disabledFallingBlockDrops.contains(fb.getBlockData().getMaterial())) {
                    if (isVisualMode(serialized)) {
                        DebrisBreakEvent dbe = new DebrisBreakEvent(fb);
                        if (dbe.callEvent()) {
                            postFall(fb, true);
                        }
                    } else {
                        DebrisBreakEvent dbe = new DebrisBreakEvent(fb, deserializeDebrisDrops(serialized));
                        if (dbe.callEvent()) {
                            dropItems(fb, dbe.getDrops());
                            postFall(fb, true);
                        }
                    }
                }
            }
        }
    }

    public NamespacedKey getFallingKey() {
        return fallingKey;
    }

    public ItemStack getTool() {
        return tool;
    }

    public boolean isSilkTouch() {
        return silkTouch;
    }
    
    public void setSilkTouch(boolean silkTouch) {
        if (silkTouch) {
            tool.addEnchantment(Enchantment.SILK_TOUCH, 1);
        } else {
            tool.removeEnchantment(Enchantment.SILK_TOUCH);
        }
        this.silkTouch = silkTouch;
    }
    
    public double getVelocityMultiplier() {
        return velocityMultiplier;
    }
    
    public void setVelocityMultiplier(double velocityMultiplier) {
        this.velocityMultiplier = velocityMultiplier;
    }
    
    public double getVelocityRandom() {
        return velocityRandom;
    }
    
    public void setVelocityRandom(double velocityRandom) {
        this.velocityRandom = velocityRandom;
    }
    
    public int getMaxRayAttempts() {
        return maxRayAttempts;
    }
    
    public void setMaxRayAttempts(int maxRayAttempts) {
        this.maxRayAttempts = maxRayAttempts;
    }
    
    public int getMaxRayDistance() {
        return maxRayDistance;
    }
    
    public void setMaxRayDistance(int maxRayDistance) {
        this.maxRayDistance = maxRayDistance;
    }
    
    public double getBreakChanceNoFree() {
        return breakChanceNoFree;
    }
    
    public void setBreakChanceNoFree(double breakChanceNoFree) {
        this.breakChanceNoFree = breakChanceNoFree;
    }
    
    public int getTicksBeforeUpdate() {
        return blockChangeSender.getDelay();
    }

    public void setTicksBeforeUpdate(int ticksBeforeUpdate) {
        blockChangeSender.close();
        blockChangeSender = new BlockChangeSender(kaboom, ticksBeforeUpdate);
    }
    
    public boolean doFallingBlocksHurtEntities() {
        return fallingBlocksHurtEntities;
    }
    
    public void setFallingBlocksHurtEntities(boolean fallingBlocksHurtEntities) {
        this.fallingBlocksHurtEntities = fallingBlocksHurtEntities;
    }
    
    public boolean getFallingBlockBreakAnimation() {
        return fallingBlockBreakAnimation;
    }
    
    public void setFallingBlockBreakAnimation(boolean fallingBlockBreakAnimation) {
        this.fallingBlockBreakAnimation = fallingBlockBreakAnimation;
    }
    
    public boolean isFallingBlockVisualOnlyMode() {
        return fallingBlockVisualOnlyMode;
    }
    
    public void setFallingBlockVisualOnlyMode(boolean fallingBlockVisualOnlyMode) {
        this.fallingBlockVisualOnlyMode = fallingBlockVisualOnlyMode;
    }
    
    public Set<Material> getDisabledFallingBlocks() {
        return disabledFallingBlocks;
    }
    
    public HashSet<Material> getDisabledFallingBlocksPlacement() {
        return disabledFallingBlocksPlacement;
    }
    
    public HashSet<Material> getDisabledFallingBlockDrops() {
        return disabledFallingBlockDrops;
    }

    @Override
    public void close() {
        blockChangeSender.close();
    }

    public Integer metricsExplodedBlocks() {
        Integer clampedExplodedBlocks = (int) Math.min(Integer.MAX_VALUE, explodedBlocks);
        explodedBlocks = 0;
        return clampedExplodedBlocks;
    }

}
