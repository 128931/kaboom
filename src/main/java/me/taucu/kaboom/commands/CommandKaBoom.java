package me.taucu.kaboom.commands;

import me.taucu.kaboom.KaBoom;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class CommandKaBoom implements CommandExecutor, TabExecutor {

    private final List<String> baseComplete = List.of("reload");

    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, String[] args) {
        if (args.length > 0) {
            if (args.length == 1) {
                return StringUtil.copyPartialMatches(args[0], baseComplete, new ArrayList<>());
            }
        } else {
            return baseComplete;
        }
        return List.of("");
    }

    @Override
    public boolean onCommand(CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        sender.sendMessage(ChatColor.AQUA + "KaBoom by TauCubed " + ChatColor.GRAY + "v" + KaBoom.get().getDescription().getVersion());
        sender.sendMessage(ChatColor.GRAY + " \u2514 " + KaBoom.get().getDescription().getDescription());
        if (args.length > 0) {
            if ("reload".equalsIgnoreCase(args[0])) {
                if (sender.hasPermission("tau.kaboom.command.reload")) {
                    KaBoom.get().loadConfig();
                    sender.sendMessage(ChatColor.AQUA + "config reloaded");
                } else {
                    sender.sendMessage("no permission");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "invalid sub-command; try /kaboom reload");
            }
        } else {
            sender.sendMessage(ChatColor.AQUA + "usage: /kaboom reload");
        }
        return true;
    }

}
