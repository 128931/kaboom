package me.taucu.kaboom;

import me.taucu.kaboom.commands.CommandKaBoom;
import me.taucu.kaboom.events.MyBlockExplodeEvent;
import me.taucu.kaboom.events.MyEntityExplodeEvent;
import me.taucu.kaboom.hooks.Hooks;
import me.taucu.kaboom.physics.ExplosionHandler;
import net.md_5.bungee.api.ChatColor;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.AdvancedPie;
import org.bstats.charts.SingleLineChart;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;

public class KaBoom extends JavaPlugin implements Listener {
    
    private static final String art = "\r\n" + ChatColor.RED
            + "     _.-^^---....,,-._     \r\n"
            + " _--                   '-_ \r\n"
            + "<                        >)\r\n"
            + "|        KA BOOM!        | \r\n"
            + " \\._                   _./\r\n"
            + "    ```--. . , ; .--'''    \r\n" + ChatColor.DARK_RED
            + "          | |   |          \r\n"
            + "       .-=||  | |=-.       \r\n"
            + "       `-=#$%&%$#=-'       \r\n"
            + "          | ;  :|          \r\n"
            + "______.,-#%&$@%#&#~,.______\r\n" + ChatColor.GRAY
            + "            By             \r\n"
            + "         TauCubed          ";
    
    private static KaBoom instance = null;
    
    public static KaBoom get() {
        return instance;
    }
    
    private final Hooks hooks = new Hooks(this);
    private ExplosionHandler handler = null;

    private Metrics metrics = null;

    @Override
    public void onEnable() {
        instance = this;

        // try to create metrics
        try {
            this.metrics = new Metrics(this, 14776);
        } catch (Exception e) {
            getLogger().log(Level.WARNING, "Error while enabling bStats metrics", e);
        }

        // initialize explosion handler
        handler = new ExplosionHandler(this);
        
        // register commands
        final CommandKaBoom cmd = new CommandKaBoom();
        final PluginCommand pcmd = getCommand("kaboom");
        Objects.requireNonNull(pcmd, "getCommand must not be null").setExecutor(cmd);
        pcmd.setTabCompleter(cmd);

        // load the config
        loadConfig();
        
        // register hooks
        hooks.enableAllHooksIn(Objects.requireNonNull(getConfig().getConfigurationSection("hooks"),"getConfigurationSection must not be null"));

        // register events
        Bukkit.getPluginManager().registerEvents(handler, this);
        Bukkit.getPluginManager().registerEvents(this, this);

        // setup metrics
        if (metrics != null) {
            metrics.addCustomChart(new AdvancedPie("block_logging_hooks", hooks::metricsBlockLoggingHooks));
            metrics.addCustomChart(new SingleLineChart("exploded_blocks", handler::metricsExplodedBlocks));
        }

        getLogger().info("Enabled KaBoom!" + art);
    }
    
    @Override
    public void onDisable() { }
    
    /**
     * Loads/Reloads the configuration from file
     */
    public void loadConfig() {
        this.saveDefaultConfig();
        this.reloadConfig();
        if (getConfig().getInt("config version") != Objects.requireNonNull(getConfig().getDefaults(), "getDefaults must not be null").getInt("config version")) {
            getLogger().warning("Your configuration is outdated. It is advisable to regenerate your configuration.");
        }
        
        final Configuration c = getConfig();
        
        handler.setVelocityMultiplier(c.getDouble("physics.velocity multiplier"));
        handler.setVelocityRandom(c.getDouble("physics.velocity random"));
        
        handler.setMaxRayAttempts(c.getInt("physics.max ray attempts"));
        handler.setMaxRayDistance(c.getInt("physics.max ray distance"));
        handler.setBreakChanceNoFree(c.getDouble("physics.break chance if no free trajectory"));
        handler.setTicksBeforeUpdate(c.getInt("physics.ticks before update"));
        
        handler.setSilkTouch(c.getBoolean("silk touch blocks"));
        
        handler.setFallingBlocksHurtEntities(c.getBoolean("physics.falling blocks hurt entities"));
        handler.setFallingBlockBreakAnimation(c.getBoolean("physics.play falling break animation"));
        
        handler.setFallingBlockVisualOnlyMode(c.getBoolean("physics.falling blocks are visual only"));
        
        handler.getDisabledFallingBlocks().clear();
        for (final String s : c.getStringList("physics.disabled falling blocks")) {
            try {
                addOrAddAll(handler.getDisabledFallingBlocks(), s);
            } catch (final IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find material by name \"" + s + "\" for \"physics.disabled falling blocks\"");
            }
        }
        
        handler.getDisabledFallingBlocksPlacement().clear();
        for (final String s : c.getStringList("physics.disabled falling blocks placement")) {
            try {
                addOrAddAll(handler.getDisabledFallingBlocksPlacement(), s);
            } catch (final IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find material by name \"" + s + "\" for \"physics.disabled falling blocks placement\"");
            }
        }
        
        handler.getDisabledFallingBlockDrops().clear();
        for (final String s : c.getStringList("physics.disabled falling block drops")) {
            try {
                addOrAddAll(handler.getDisabledFallingBlockDrops(), s);
            } catch (final IllegalArgumentException e) {
                getLogger().log(Level.WARNING, "Couldn't find material by name \"" + s + "\" for \"physics.disabled falling block drops\"");
            }
        }
    }
    
    /**
     * Adds a {@link Material} by name or every {@link Material} if the value is "EVERYTHING"
     * 
     * @param set The {@link Set} to add values to
     * @param value   The value
     * @throws IllegalArgumentException If an invalid argument is used
     */
    private void addOrAddAll(Set<Material> set, String value) throws IllegalArgumentException {
        if (value.equalsIgnoreCase("EVERYTHING")) {
            set.addAll(Arrays.asList(Material.values()));
        } else {
            set.add(Material.valueOf(value));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockExplode(BlockExplodeEvent e) {
        if (e.getClass() == BlockExplodeEvent.class) {
            e = new MyBlockExplodeEvent(e.getBlock(), e.blockList(), e.getYield());
            if (e.callEvent()) {
                handler.handleExplosion(e.getBlock().getLocation().toCenterLocation(), e.blockList());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent e) {
        if (e.getClass() == EntityExplodeEvent.class) {
            e = new MyEntityExplodeEvent(e.getEntity(), e.getLocation(), e.blockList(), e.getYield());
            if (e.callEvent()) {
                handler.handleExplosion(e.getLocation().clone().add(0, 0.5, 0), e.blockList());
            }
        }
    }

    /**
     * Get the Hook api
     *
     * @return the instance of the {@link Hooks} api
     */
    public Hooks getHooks() {
        return hooks;
    }

    /**
     * Get the Explosion Handler
     *
     * @return the instance of the {@link ExplosionHandler}
     */
    public ExplosionHandler getHandler() {
        return handler;
    }
    
}
