package me.taucu.kaboom.hooks;

import me.taucu.kaboom.KaBoom;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import java.util.*;
import java.util.logging.Level;

public record Hooks(KaBoom kaboom) {

    private static final IdentityHashMap<Class<? extends Hook>, Hook> enabledHooks = new IdentityHashMap<>();

    public void enableAllHooksIn(ConfigurationSection hooks) {
        disableAllHooks();
        for (String k : hooks.getKeys(false)) {
            if (hooks.isBoolean(k) && hooks.getBoolean(k)) {
                switch (k.toLowerCase()) {
                    case "coreprotect hook":
                        if (checkPlugin("CoreProtect")) {
                            enableHook(new CoreProtectHook(kaboom));
                        } else {
                            kaboom.getLogger().warning("Cannot enable coreprotect hook. CoreProtect plugin not found.");
                        }
                        break;
                    case "logblock hook":
                        if (checkPlugin("LogBlock")) {
                            enableHook(new LogBlockHook(kaboom));
                        } else {
                            kaboom.getLogger().warning("Cannot enable logblock hook. LogBlock plugin not found.");
                        }
                        break;
                    case "prism hook":
                        if (checkPlugin("Prism")) {
                            enableHook(new PrismHook(kaboom));
                        } else {
                            kaboom.getLogger().warning("Cannot enable prism hook. Prism plugin not found.");
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void disableAllHooks() {
        for (Hook h : getEnabledHooks()) {
            disableHook(h);
        }
        enabledHooks.clear();
    }

    public void enableHook(Hook h) {
        if (!enabledHooks.containsKey(h.getClass())) {
            try {
                enabledHooks.put(h.getClass(), h);
                h.onEnable();
            } catch (Throwable t) {
                kaboom.getLogger().log(Level.SEVERE, "Error while enabling hook \"" + h.getClass().getSimpleName() + "\"", t);
                disableHook(h);
            }
        }
    }

    public void disableHook(Hook h) {
        enabledHooks.remove(h.getClass());
        try {
            h.onDisable();
        } catch (Throwable t) {
            kaboom.getLogger().log(Level.SEVERE, "Error while disabling hook \"" + h.getClass().getSimpleName() + "\"", t);
        }
    }

    public static Set<Hook> getEnabledHooks() {
        return new HashSet<>(enabledHooks.values());
    }

    private static boolean checkPlugin(String p) {
        return Bukkit.getPluginManager().isPluginEnabled(p);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Hook> T getHook(Class<T> clazz) {
        return (T) enabledHooks.get(clazz);
    }

    public Map<String, Integer> metricsBlockLoggingHooks() {
        HashMap<String, Integer> map = new HashMap<>();
        if (getHook(CoreProtectHook.class) != null) map.put("CoreProtect", 1);
        if (getHook(LogBlockHook.class) != null) map.put("LogBlock", 1);
        if (getHook(PrismHook.class) != null) map.put("Prism", 1);

        if (map.isEmpty()) map.put("None", 1);

        return map;
    }

}
