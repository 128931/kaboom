package me.taucu.kaboom.hooks;

import me.taucu.kaboom.KaBoom;

public abstract class Hook {
    
    public abstract void onEnable();
    
    public abstract void onDisable();

    public void enable() {
        KaBoom.get().getHooks().enableHook(this);
    }

    public void disable() {
        KaBoom.get().getHooks().disableHook(this);
    }
    
}
