# About

KaBoom is my implementation of explosion physics in Minecraft.

It works for TNT, Creepers, Fireballs and should work for any other Explosion. It is requires a Paper Spigot server to
work.

Currently, supported Minecraft versions are 1.16 - 1.18

# Features

- Silk touch option for explosions.
- Disable or Enable debris.
- Enable debris as visual effect only
- Debris drop their block's loot tables.
- Debris can deal damage to entities they hit.
- Debris landing sound effects.
- Ability to set if debris should play break particles if they fail to land.
- Ability to set whether to place specific or all types of debris.
- Ability to set whether to drop items for specific or all types of debris.
- Explosion physics are dynamically calculated.
- CoreProtect support.

# To Use

- Stop server (if started)
- Add KaBoom binary to ./plugins
- Start server

# Configuration

- The configuration is located in ./plugins/kaboom/config.yml
- Use **/kaboom reload** to reload configuration from file.

# Building

To build this project either use the included gitlab-ci or via `mvn clean package`

The build binaries will output to `./target/KaBoom-<version>.jar`
